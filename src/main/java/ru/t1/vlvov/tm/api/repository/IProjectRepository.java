package ru.t1.vlvov.tm.api.repository;

import ru.t1.vlvov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

}
