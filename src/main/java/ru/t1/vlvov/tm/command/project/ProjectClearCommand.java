package ru.t1.vlvov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Remove all projects.";

    @NotNull
    private final String NAME = "project-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @Nullable final String userId = getAuthService().getUserId();
        getProjectService().clear(userId);
    }

}
